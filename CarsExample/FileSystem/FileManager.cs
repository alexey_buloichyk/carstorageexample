﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Diagnostics;

namespace StorageExample.FileSystem
{
    public static class FileManager
    {
        private static readonly string fileFolderBasePath = ConfigurationManager.AppSettings["fileFolderBasePath"];

        public static Guid Save(byte[] data)
        {
            if (data.Length > 20000000) // near 20MB
            {
                throw new InvalidOperationException("File size is too big (allwed files with size less then 20MB).");
            }

            var id = Guid.NewGuid();
            using (var fileStream = File.Create(Path.Combine(fileFolderBasePath, id.ToString())))
            {
                fileStream.Write(data, 0, data.Length);
            }
            return id;
        }

        public static void Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                return;
            }

            File.Delete(GetPath(id));
        }

        public static byte[] Get(Guid id)
        {
            var path = GetPath(id);
            using (var fileStream = File.OpenRead(path))
            {
                var length = (int)fileStream.Length;
                var buffer = new byte[length];
                fileStream.Read(buffer, 0, length);
                return buffer;
            }
        }

        private static string GetPath(Guid id)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileFolderBasePath, id.ToString());
        }
    }
}