﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using StorageExample.Models;

namespace StorageExample.DataAccess
{
    public class StorageContext : DbContext 
    {
        public StorageContext()
            : base("name=StorageDBConnectionString") 
        {
            Database.SetInitializer(new StorageContextInitializer());
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<CarModel> CarModels { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
    }
}