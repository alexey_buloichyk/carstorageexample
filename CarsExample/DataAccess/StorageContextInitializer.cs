﻿using StorageExample.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StorageExample.DataAccess
{
    public class StorageContextInitializer : DropCreateDatabaseAlways<StorageContext>
    {
        protected override void Seed(StorageContext context)
        {
            context.Manufacturers.AddRange(new List<Manufacturer> {
                new Manufacturer() { ManufacturerId = 1, Name = "Audi" },
                new Manufacturer() { ManufacturerId = 2, Name = "Hyundai" },
                new Manufacturer() { ManufacturerId = 3, Name = "BMW" },
            });

            context.CarModels.AddRange(new List<CarModel> {
                new CarModel() { CarModelId = 1, ManufacturerId = 1, Name = "A5" },
                new CarModel() { CarModelId = 2, ManufacturerId = 1, Name = "A8" },
                new CarModel() { CarModelId = 3, ManufacturerId = 2, Name = "Accent" },
                new CarModel() { CarModelId = 4, ManufacturerId = 2, Name = "Genesis" },
                new CarModel() { CarModelId = 5, ManufacturerId = 2, Name = "Solaris" },
                new CarModel() { CarModelId = 6, ManufacturerId = 3, Name = "X5" },
            });

            context.Cars.AddRange(new List<Car> {
                new Car() { CarId = 1, CarModelId = 1, MaxSpeed = 400, Mileage = 0, Price = 40000, VIN = "11111111111111111" },
                new Car() { CarId = 2, CarModelId = 1, MaxSpeed = 400, Mileage = 0, Price = 40000, VIN = "11111111111111112" },
                new Car() { CarId = 3, CarModelId = 2, MaxSpeed = 200, Mileage = 300000, Price = 3000, VIN = "11111111111111113" },
                new Car() { CarId = 4, CarModelId = 3, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111114" },
                new Car() { CarId = 5, CarModelId = 4, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111115" },
                new Car() { CarId = 6, CarModelId = 4, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111116" },
                new Car() { CarId = 7, CarModelId = 4, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111117" },
                new Car() { CarId = 8, CarModelId = 4, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111118" },
                new Car() { CarId = 9, CarModelId = 4, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111119" },
                new Car() { CarId = 10, CarModelId = 4, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111120" },
                new Car() { CarId = 11, CarModelId = 5, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111121" },
                new Car() { CarId = 12, CarModelId = 6, MaxSpeed = 200, Mileage = 0, Price = 40000, VIN = "11111111111111122" },
            });

            base.Seed(context);
        }
    }
}