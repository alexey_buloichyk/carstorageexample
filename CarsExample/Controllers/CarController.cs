﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Collections.Generic;
using System.Data;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using StorageExample.Models;
using StorageExample.DataAccess;
using StorageExample.Helpers;
using System.IO;
using System.Diagnostics;
using System.Linq.Expressions;
using StorageExample.FileSystem;
using CarsExample.Helpers;

namespace StorageExample.Controllers
{
    public class CarController : Controller
    {
        private static readonly Dictionary<string, Expression<Func<Car, object>>> _carExpressions;

        static CarController()
        {
            _carExpressions = new Dictionary<string, Expression<Func<Car, object>>>();
            _carExpressions.Add("Manufacturer", c => c.CarModel.Manufacturer.Name);
            _carExpressions.Add("CarModel", c => c.CarModel.Name);
        }

        private StorageContext db = new StorageContext();

        // GET: /Car/
        public ActionResult Index()
        {
            var cars = db.Cars.Include(c => c.CarModel);

            ViewBag.carModelsList = SelectListHelper.GetCarModelsList();
            return View(cars.ToList());
        }

        // GET: /Car/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // GET: /Car/Create
        public ActionResult Create()
        {
            ViewBag.CarModelId = new SelectList(db.CarModels, "CarModelId", "Name");
            return View();
        }

        // POST: /Car/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Car car)
        {
            if (ModelState.IsValid)
            {
                car.Photo = FileHelper.GetUploadedFileId(Request);
                db.Cars.Add(car);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CarModelId = new SelectList(db.CarModels, "CarModelId", "Name", car.CarModelId);
            return View(car);
        }

        // GET: /Car/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            ViewBag.CarModelId = new SelectList(db.CarModels, "CarModelId", "Name", car.CarModelId);
            return View(car);
        }

        // POST: /Car/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Car car)
        {
            if (ModelState.IsValid)
            {
                db.Entry(car).State = EntityState.Modified;
                car.Photo = FileHelper.GetUploadedFileId(Request);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CarModelId = new SelectList(db.CarModels, "CarModelId", "Name", car.CarModelId);
            return View(car);
        }

        // GET: /Car/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: /Car/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Car car = db.Cars.Find(id);
            db.Cars.Remove(car);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult LoadData()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            sortColumn = !string.IsNullOrEmpty(sortColumn) ? sortColumn : "Price";

            //Paging Size
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            // Getting all data
            var carsData = (from c in db.Cars.Include(c => c.CarModel.Manufacturer) select c);

            //Sorting
            if (_carExpressions.ContainsKey(sortColumn))
            {
                if (sortColumnDir != "desc")
                {
                    carsData = carsData.OrderBy(_carExpressions[sortColumn]);
                }
                else
                {
                    carsData = carsData.OrderByDescending(_carExpressions[sortColumn]);
                }
            }
            else
            {
                carsData = carsData.OrderBy(sortColumn + " " + sortColumnDir);
            }
            
            ////Search
            if (!string.IsNullOrEmpty(searchValue))
            {
                carsData = carsData.Where(
                    m => m.CarModel.Manufacturer.Name.Contains(searchValue) || m.CarModel.Name.Contains(searchValue));
            }

            recordsTotal = carsData.Count();
            var data = carsData.Skip(skip).Take(pageSize).Select(
                car => new {
                    CarId = car.CarId,
                    Manufacturer = car.CarModel.Manufacturer.Name,
                    CarModel = car.CarModel.Name,
                    Price = car.Price,
                    Mileage = car.Mileage,
                    MaxSpeed = car.MaxSpeed,
                    VIN = car.VIN,
                    Photo = car.Photo,
                })
                .ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
