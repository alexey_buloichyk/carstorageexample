﻿using StorageExample.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StorageExample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("/Car/Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "My application description page.";

            return View();
        }
    }
}