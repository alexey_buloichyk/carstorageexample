﻿using StorageExample.FileSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StorageExample.Controllers
{
    public class ImageController : Controller
    {
        //
        // GET: /Image/id
        public ActionResult Get(Guid id)
        {
            return new FileContentResult(FileManager.Get(id), "image/jpeg");
        }
	}
}