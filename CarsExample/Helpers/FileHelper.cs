﻿using StorageExample.FileSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarsExample.Helpers
{
    public static class FileHelper
    {
        public static Guid GetUploadedFileId(HttpRequestBase request)
        {
            if (request.Files.Count > 0)
            {
                var file = request.Files[0];

                var length = (int)file.InputStream.Length;
                if (length > 0)
                {
                    var buffer = new byte[length];
                    file.InputStream.Read(buffer, 0, length);
                    return FileManager.Save(buffer);
                }
            }
            return Guid.Empty;
        }
    }
}