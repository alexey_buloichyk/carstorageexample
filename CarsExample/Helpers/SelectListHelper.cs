﻿using StorageExample.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StorageExample.Helpers
{
    public static class SelectListHelper
    {
        public static SelectList GetCarModelsList(int manufacturerId = 0)
        {
            using(StorageContext db = new StorageContext())
            {
                var query = db.CarModels;

                if (manufacturerId != 0)
                {
                    query.Where(m => m.ManufacturerId == manufacturerId);
                }

                var carModelsResult = query
                    .Select(
                        m => new { 
                            id = m.CarModelId,
                            name = m.Name,
                        })
                    .ToList();
                
                return new SelectList(carModelsResult, "id", "name");
            }
        }
    }
}