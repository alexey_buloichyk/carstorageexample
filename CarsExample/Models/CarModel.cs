﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace StorageExample.Models
{
    public class CarModel
    {
        public int CarModelId { get; set; }
        [StringLength(50), Index("UX_CarModel_Name", IsUnique = true)]
        public string Name { get; set; }

        [Required]
        public int ManufacturerId { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        [XmlIgnore]
        public virtual List<Car> Cars { get; set; }
    }
}