﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace StorageExample.Models
{
    public class Car
    {
        public int CarId { get; set; }
        [Required, Index("UX_Car_VIN", IsUnique = true)]
        [StringLength(17, MinimumLength = 17)]
        public string VIN { get; set; }
        public decimal Price { get; set; }
        public int Mileage { get; set; }
        public int MaxSpeed { get; set; }
        public Guid Photo { get; set; }
        
        [NotMapped]
        public HttpPostedFileBase PhotoFile { get; set; }

        [Required, Display(Name = "Car model")]
        public int CarModelId { get; set; }
        
        public virtual CarModel CarModel { get; set; } 
    }
}