﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace StorageExample.Models
{
    public class Manufacturer
    {
        public int ManufacturerId { get; set; }
        [Index("UX_Manufacturer_Name", IsUnique = true)]
        [StringLength(50)]
        public string Name { get; set; }
        [XmlIgnore]
        public virtual List<CarModel> CarModels { get; set; }
    }
}